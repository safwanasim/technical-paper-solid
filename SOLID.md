**SOLID** is an acronym that stands for five key design principles: **single responsibility principle, open-closed principle, Liskov substitution principle, interface segregation principle, and dependency inversion principle**. The SOLID principle was introduced by **Robert C. Martin**, also known as *Uncle Bob* and it is a coding standard in programming.
All five are commonly used by software engineers and provide some important benefits for developers. The principles are as follows:
1. **Single Responsibility Principle (SRP)**
2. **Open/Closed Principle**
3. **Liskov’s Substitution Principle (LSP)**
4. **Interface Segregation Principle (ISP)**
5. **Dependency Inversion Principle (DIP)**

##### Single Responsibility Principle (SRP):
Robert C. Martin describes it as: *A class should have one, and only one, reason to change.* This principle statest that every class should have a single responsibility or single job or single purpose. This means that the code should only have one single reason to change. If there are two different reasons for the code to change, or if it does two distinctly different things then it is in violation of this principle and is potentially poorly designed code.

``` 
class CalorieTacker{
    constructor(maxCalories){
        this.maxCalories = maxCalories
        this.currentCalorues = 0
    }
}
trackCalories(calorieCount){
    this.currentCalories += calorieCount
    if(this.currentCalories > this.maxCalories){
        this.logCalorieSurplus()
    }
}

    logCalorieSurplus(){
    console.log("Max calorie exceeded")
    }
}

const calorieTracker = new CalorieTracker(2000)
calorieTrracker.trackCalories(500)
calorieTracker.trackCalories(1000)
calorieTracker.trackCalories(700)
```
If we want to change how we notify the user, we need to change class and if we need to track the calories we need to change class again. So, we're gonna move this logging into its own module that has one responsibilty which is logging.

```
class CalorieTacker{
    constructor(maxCalories){
        this.maxCalories = maxCalories
        this.currentCalorues = 0
    }
}
trackCalories(calorieCount){
    this.currentCalories += calorieCount
    if(this.currentCalories > this.maxCalories){
        this.logCalorieSurplus()
    }
}

const calorieTracker = new CalorieTracker(2000)
calorieTrracker.trackCalories(500)
calorieTracker.trackCalories(1000)
calorieTracker.trackCalories(700)

function logMessage(message){
    //email the user
}
```
##### Open/Closed Principle:
In object-oriented programming, the open–closed principle states "software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification" - that is, such an entity can allow its behaviour to be extended without modifying its source code.
```
public class BasicUser : IUser
        {
            public int Id { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public IPlanAccount PlanAccount { get; set; } = new BasicAccount();
        }

        public class IntermediateUser : IUser
        {
            public int Id { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public IPlanAccount PlanAccount { get; set; } = new IntermediateAccount();
        }

        public class PremiumUser : IUser
        {
            public int Id { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public IPlanAccount PlanAccount { get; set; } = new PremiumAccount();
        }
```
##### Liskov Substitution:
*Derived or child classes must be substitutable for their base or parent classes*. The main idea of the Liskov substitution principle is that any function/module that interacts with a class should also be able to interact with all subclasses of that class without breaking. This essentially means that a class is interchangeable with its subclasses.

```
class Rectangle{
    constructor(width, height){
        this.width =width
        this.height = height
    }
    setWidth(width){
        this.width = width
    }
    setHeight(height){
        this.height = height
    }
    area(){
        return this.width * this.height
    }
}

class Square extends Rectangle{
    setWidth(width){
        this.width= width
        this.height = width
    }
    setHeight(height){
        this.height = height
        this.width = width
    }
    area(){
        return this.width * this.height
    }
}
```
Our subclass of square cannot be substituted in place of rectangle so we now are failing that Liskov Substituion principle and this needs to be fixed. We have to change what we're inheriting from so we want to create another class that is just for example shape and this shape class here would actually just have area function and our rectangle is going to extend shape and our square is also going to extend shape.

```
class Rectangle extends Shape{
    constructor(width, height){
        this.width = width
        this.height = height
    }
    setWidth(width){
        this.width = width
    }
    setHeight(height){
        this.height=height
    }
    area(){
        return this.width * this.height
    }
}
```

##### Interface-Segregation Principle
In the field of software engineering, the interface-segregation principle (ISP) states that no code should be forced to depend on methods it does not use. ISP splits interfaces that are very large into smaller and more specific ones so that clients will only have to know about the methods that are of interest to them. Such shrunken interfaces are also called role interfaces. ISP is intended to keep a system decoupled and thus easier to refactor, change, and redeploy.

```
interface Entity{
    attackDamage
    health
    name

    move()
    attack()
    takeDamage(amount)
}

class Character implements Entity{
    move(){
        //Do Something
    }
    attack(){
        //Do something
    }
    takeDamage(amount){
        //Do something
    }
}

class Turret implements Entity{
    move(){
        //Error: Cannot move
    }
}

```
In the above example we can see the Character implementing all the functionalities of above interface but Turret doesn't fully utilize the interface, in the sense that move functionality of interface cannot be assigned to Turret. To fix this, we do the following, we break apart the entity class to smaller fragments so that we implement only what we need

```
class Turret extends Entity{
    constructor(name, attackDamage){
        super(name)
        this.attackDamage = attackDamage
    }
}
Object.assign(Turret.prototype, attacker)
```
Here, we only assign the functionality that turret works on to turret, we donot assign it unnecessary functionality like move() - a thing that turret doesnt do.

##### Dependency Inversion Principle (DIP)
The main idea of the dependency inversion principle is that any class that uses a dependency should only ever use the dependency through a predefined interface/wrapper. This makes it so that your code will never directly depend on a low level API for its operations. The reason this is so important is because if you ever need to change or remove that dependency it becomes really difficult when it is used all over your code. By wrapping this dependency in an interface you can depend on the interface you created which will make changing out the dependency painless.


```
class Store{
    constructor(user){
        this.stripe = new Stripe(user)
    }
    
    purchasedBike(quantity){
        this.stripe.makePayment(200*quantity*100)
    }
    purchaseHelmet(quantity){
        this.stripe.makePayment(15*quantity*100)
    }
    
}

class Stripe{
    constructor(user){
        this.user= user
    }
    makePayment(amountInCents){
        console.log(`${this.user} made payment of ${amountInCents/100} with stripe`)
    }
}

class Paypal{
    makePayment(user, amountInDollars){
        console.log(`${user} made payment of ${amountInDollars} wih Paypal`)
    }
}

const store = new Store('John')
store.purchaseBike(2)
store.purchaseHelemet(2)
```

We need a payment processor that wraps around the stripe and paypal payment objects that we can use so that our code becomes more simpler in case we want to switch between paypal and stripe

```
class store{
    constructor(user){
        this.paymentProcessor = new PaymentProcessor(user)
    }
    purchasedBike(quantity){
        this.paymentProcessor.pay(200*quantity)
    }
    purchaseHelmet(quantity){
        this.paymentProcessor.pay(15*quantity)
    }
}

class StripePaymentProcessor{
    constructor(user){
        this.stripe= new Stripe(user)
    }
    pay(amoountInDollars){
        this.stripe.makePayment(amountInDollars * 100)
    }
}

class Stripe{
    constructor(user){
        this.user= user
    }
    makePayment(amountInCents){
        console.log(`${this.user} made payment of ${amountInCents/100} with stripe`)
    }
}

class PaypalPaymentProcessor{
    constructor(user){
        this.user = user
        this.paypal = new Paypal
    }
    pay(amountInDollars){
        this.paypal.makePayment(this.user, amountInDollars)
    }
}

class Paypal{
    makePayment(user, amountInDollars){
        console.log(`${user} made payment of ${amountInDollars} wih Paypal`)
    }
}
const store = new Store('John')
store.purchaseBike(2)
store.purchaseHelemet(2)
```


References:
1. https://en.wikipedia.org/wiki/SOLID
2. https://www.youtube.com/watch?v=HLFbeC78YlU
